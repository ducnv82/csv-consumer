package com.mycompany.csvconsumer.service;

import java.util.List;

import com.mycompany.csvconsumer.model.AccountDetail;
import com.mycompany.csvconsumer.model.AnalyzedResult;
import org.junit.Test;

import static java.lang.System.lineSeparator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CSVServiceTest {

    private CSVService csvService = new CSVServiceImpl();

    final String csvContents =
        "0977dca4-9906-3171-bcec-87ec0df9d745,kFFzW4O8gXURgP8ShsZ0gcnNT5E=,0.18715484122922377,982761284,8442009284719321817"  + lineSeparator() +
        "5fac6dc8-ea26-3762-8575-f279fe5e5f51,cBKFTwsXHjwypiPkaq3xTr8UoRE=,0.7626710614484215,1005421520,6642446482729493998"  + lineSeparator() +
        "0977dca4-9906-3171-bcec-87ec0df9d745,9ZWcYIblJ7ebN5gATdzzi4e8K7Q=,0.9655429720343038,237475359,3923415930816731861"   + lineSeparator() +
        "4d968baa-fe56-3ba0-b142-be9f457c9ff4,RnJNTKLYpcUqhjOey+wEIGHC7aw=,0.6532229483547558,1403876285,4756900066502959030"  + lineSeparator() +
        "0977dca4-9906-3171-bcec-87ec0df9d745,N0fiZEPBjr3bEHn+AHnpy7I1RWo=,0.8857966322563835,1851028776,6448117095479201352"  + lineSeparator() +
        "0977dca4-9906-3171-bcec-87ec0df9d745,P/wNtfFfa8jIn0OyeiS1tFvpORc=,0.8851653165728414,1163597258,8294506528003481004"  + lineSeparator() +
        "0977dca4-9906-3171-bcec-87ec0df9d745,Aizem/PgVMKsulLGquCAsLj674U=,0.5869654624020274,1012454779,2450005343631151248"  + lineSeparator() +
        "023316ec-c4a6-3e88-a2f3-1ad398172ada,TRQb8nSQEZOA5Ccx8NntYuqDPOw=,0.3790267017026414,652953292,4677453911100967584"   + lineSeparator() +
        "023316ec-c4a6-3e88-a2f3-1ad398172ada,UfL8VetarqYZparwV4AJtyXGgFM=,0.26029423666931595,1579431460,5620969177909661735" + lineSeparator() +
        "0977dca4-9906-3171-bcec-87ec0df9d745,uZNIcWQtwst+9mjQgPkV2rvm7QY=,0.039107542861771316,280709214,4450245425875000740" + lineSeparator();

    @Test
    public void testConsumeCSV() throws Exception {
        final AnalyzedResult result = csvService.consumeCSV(csvContents, 1, false).get();
        assertNotNull(result);

        final List<AccountDetail> accountDetails = result.getAccountDetails();

        assertEquals("55706069246767970369", result.getSum());
        assertEquals(4, accountDetails.size());

        for (AccountDetail accountDetail : accountDetails) {
            if ("023316ec-c4a6-3e88-a2f3-1ad398172ada".equalsIgnoreCase(accountDetail.getAccountNo())) {
                assertEquals(0.31966, accountDetail.getAverage(), 0.001);
                assertEquals(1579431460, accountDetail.getMostRecentValue());
            }

            if ("4d968baa-fe56-3ba0-b142-be9f457c9ff4".equalsIgnoreCase(accountDetail.getAccountNo())) {
                assertEquals(0.65322, accountDetail.getAverage(), 0.001);
                assertEquals(1403876285, accountDetail.getMostRecentValue());
            }

            if ("5fac6dc8-ea26-3762-8575-f279fe5e5f51".equalsIgnoreCase(accountDetail.getAccountNo())) {
                assertEquals(0.76267, accountDetail.getAverage(), 0.001);
                assertEquals(1005421520, accountDetail.getMostRecentValue());
            }

            if ("0977dca4-9906-3171-bcec-87ec0df9d745".equalsIgnoreCase(accountDetail.getAccountNo())) {
                assertEquals(0.59162, accountDetail.getAverage(), 0.001);
                assertEquals(280709214, accountDetail.getMostRecentValue());
            }
        }
    }
}
