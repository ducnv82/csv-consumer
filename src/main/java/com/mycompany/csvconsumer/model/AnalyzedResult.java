package com.mycompany.csvconsumer.model;

import java.util.List;

import static java.lang.System.lineSeparator;

public class AnalyzedResult {

    private final String sum;
    private final List<AccountDetail> accountDetails;

    public AnalyzedResult(final String sum, final List<AccountDetail> accountDetails) {
        this.sum = sum;
        this.accountDetails = accountDetails;
    }

    public String getSum() {
        return sum;
    }

    public List<AccountDetail> getAccountDetails() {
        return accountDetails;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(sum).append(lineSeparator()).append(accountDetails.size())
                                                            .append(lineSeparator());
        for (AccountDetail accountDetail : accountDetails) {
            builder.append(accountDetail.getAccountNo()).append(",")
                    .append(accountDetail.getAverage()).append(",").append(accountDetail.getMostRecentValue()).append(
                    lineSeparator());
        }

        return builder.toString();
    }
}
