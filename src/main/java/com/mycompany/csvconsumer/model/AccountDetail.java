package com.mycompany.csvconsumer.model;

public class AccountDetail {

    private final String accountNo;
    private final double average;
    private final int mostRecentValue;

    public AccountDetail(final String accountNo, final double average, final int mostRecentValue) {
        this.accountNo = accountNo;
        this.average = average;
        this.mostRecentValue = mostRecentValue;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public double getAverage() {
        return average;
    }

    public int getMostRecentValue() {
        return mostRecentValue;
    }
}
