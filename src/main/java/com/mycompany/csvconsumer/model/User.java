package com.mycompany.csvconsumer.model;

import java.math.BigDecimal;

import com.univocity.parsers.annotations.Parsed;

public class User {

    @Parsed(index = 0)
    private String accountNo;

    @Parsed(index = 1)
    private String password;

    @Parsed(index = 2)
    private double salary;

    @Parsed(index = 3)
    private int boughtCoffeeCups;

    @Parsed(index = 4)
    private BigDecimal boughtBreads;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(final String accountNo) {
        this.accountNo = accountNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(final double salary) {
        this.salary = salary;
    }

    public int getBoughtCoffeeCups() {
        return boughtCoffeeCups;
    }

    public void setBoughtCoffeeCups(final int boughtCoffeeCups) {
        this.boughtCoffeeCups = boughtCoffeeCups;
    }

    public BigDecimal getBoughtBreads() {
        return boughtBreads;
    }

    public void setBoughtBreads(final BigDecimal boughtBreads) {
        this.boughtBreads = boughtBreads;
    }
}
