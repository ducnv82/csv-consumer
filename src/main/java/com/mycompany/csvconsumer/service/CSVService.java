package com.mycompany.csvconsumer.service;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import com.mycompany.csvconsumer.model.AnalyzedResult;

public interface CSVService {

    CompletableFuture<AnalyzedResult> consumeCSV(String csvContents, double batch, boolean writeToFile) throws IOException;
}
