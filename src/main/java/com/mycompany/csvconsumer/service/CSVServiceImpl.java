package com.mycompany.csvconsumer.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;

import com.mycompany.csvconsumer.model.AccountDetail;
import com.mycompany.csvconsumer.model.AnalyzedResult;
import com.mycompany.csvconsumer.model.User;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static java.math.BigDecimal.ZERO;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

@Service
public class CSVServiceImpl implements CSVService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CSVServiceImpl.class);

    @Value("${file.output.path:.}")
    private String fileOutputPath;

    @Async
    @Override
    public CompletableFuture<AnalyzedResult> consumeCSV(final String csvContents, final double batch, final boolean writeToFile) throws IOException {
        final List<User> users = parseCSVContents(csvContents);
        final BigDecimal sumOfBoughtBreads = users.stream().map(User::getBoughtBreads).reduce(ZERO, BigDecimal::add);
        final Map<String, List<Double>> salariesByAccountNo = users.stream().collect(groupingBy(User::getAccountNo, mapping(User::getSalary, toList())));
        final Map<String, List<Integer>> boughtCoffeeCupsByAccountNo = users.stream().collect(groupingBy(User::getAccountNo, mapping(User::getBoughtCoffeeCups, toList())));

        LOGGER.debug("batch: {}", batch);
        LOGGER.debug("sum of data point 5 overall: {}", sumOfBoughtBreads.toPlainString());
        LOGGER.debug("number of unique users: {}", salariesByAccountNo.keySet().size());

        final List<AccountDetail> accountDetails = new ArrayList<>();
        for (Entry<String, List<Double>> salariesByAccountNoEntry : salariesByAccountNo.entrySet()) {
            String accountNo = salariesByAccountNoEntry.getKey();
            double averageSalary = salariesByAccountNoEntry.getValue().stream().mapToDouble(salary -> salary).average().orElse(0);
            List<Integer> boughtCoffeeCups = boughtCoffeeCupsByAccountNo.get(accountNo);

            accountDetails.add(new AccountDetail(accountNo, averageSalary, boughtCoffeeCups.get(boughtCoffeeCups.size() - 1)));

            LOGGER.debug("average value of data point 3 of the user {}: {}", accountNo, averageSalary);
        }

        final AnalyzedResult analyzedResult = new AnalyzedResult(sumOfBoughtBreads.toPlainString(), accountDetails);

        if (writeToFile) {
            writeToFile(batch, analyzedResult);
        }

        return completedFuture(analyzedResult);
    }

    private void writeToFile(final double batch, final AnalyzedResult analyzedResult) throws IOException {
        try (FileWriter file = new FileWriter(fileOutputPath + "/" + batch + ".txt")) {
            file.write(analyzedResult.toString());
        }
    }

    private List<User> parseCSVContents(final String csvContents) {
        final BeanListProcessor<User> rowProcessor = new BeanListProcessor<>(User.class);
        final CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(false);
        parserSettings.setDelimiterDetectionEnabled(true, ',');
        final CsvParser parser = new CsvParser(parserSettings);
        parser.parse(new StringReader(csvContents));

        return rowProcessor.getBeans();
    }
}
