package com.mycompany.csvconsumer.config;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAdder;

import com.mycompany.csvconsumer.service.CSVService;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.lang.System.lineSeparator;
import static java.net.InetAddress.getLocalHost;

@Component
public class CamelRoutes extends RouteBuilder {

    @Autowired
    private CSVService csvService;

    @Override
    public void configure() throws Exception {
//        from("netty4:tcp://192.168.0.9:9000?textline=true").to("log:result");

        final AtomicInteger linesCount = new AtomicInteger(0);
        final DoubleAdder batchCount = new DoubleAdder();
        final StringBuilder linesBuilder = new StringBuilder();

        from("netty4:tcp://" + getLocalHost().getHostAddress() + ":9000?textline=true").process(exchange -> {
            linesBuilder.append(exchange.getIn().getBody()).append(lineSeparator());
            if (linesCount.incrementAndGet() == 1000) {
                batchCount.add(1);
                csvService.consumeCSV(linesBuilder.toString(), batchCount.doubleValue(), true);
                linesBuilder.setLength(0);
                linesCount.set(0);
            }
        });
    }
}
