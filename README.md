# CSV Consumer application

# Requirements: <https://github.com/scravy/exercise>

# Run the application

1. Installations: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Download and install latest Apache Maven (or at least Maven 3.6.1)
3. In the root of the application type `mvn spring-boot:run` to run the app. Type `Ctrl+C` to shutdown the app.
4. Run the app producer `java -jar producer.jar --tcp` to send data to port 9000 via TCP.
5. In the root of the application type `mvn test` to run the unit test.
6. By default, output files are in the application root folder, to change it, modify the property `file.output.path` in the file `src/main/resources/application.properties` and run the app again by typing `mvn clean spring-boot:run`


# Architecture

*   The application is written in Spring Boot 2.1, Apache Camel 2.24 with Netty 4 to consume data from port 9000.
*   The class CamelRoutes contains logic to consume data from the port 9000 and calls CSVService#consumeCSV to process CSV data. CSVService#consumeCSV is a method with asynchronous processing (Spring async), extremely fast and reliable CSV parser: univocity-parsers to parse CSV and map data to JavaBean.
  
Feel free to discuss by dropping e-mails to <ducnv82@gmail.com>
 